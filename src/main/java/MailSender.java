import config.MailType;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import service.MailService;
import service.MailServiceFactory;

public class MailSender extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("MAILENATOR-3000");
        primaryStage.setMinWidth(512);
        primaryStage.setMinHeight(512);

        TabPane tabPane = new TabPane();
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));

        Tab sendTab = new Tab("Send email");
        sendTab.setClosable(false);
        sendTab.setContent(gridPane);

        TextField sendToField = new TextField();
        TextField topicField = new TextField();
        TextArea messageArea = new TextArea();

        Label sendToLabel = new Label("Send to: ");
        Label topicLabel = new Label("Topic: ");
        Label messageLabel = new Label("Message: ");

        Button button = new Button("Send");
        HBox box = new HBox(10);
        box.getChildren().add(button);
        box.setAlignment(Pos.BASELINE_RIGHT);

        ToggleGroup toggleGroup = new ToggleGroup();

        RadioButton realToggle = new RadioButton("Real");
        realToggle.setToggleGroup(toggleGroup);
        realToggle.setSelected(false);
        realToggle.setUserData(MailType.REAL);

        RadioButton mockToggle = new RadioButton("Mock");
        mockToggle.setToggleGroup(toggleGroup);
        mockToggle.setSelected(true);
        mockToggle.setUserData(MailType.MOCK);

        HBox toggleBox = new HBox(15, mockToggle, realToggle);

        gridPane.add(sendToLabel, 0, 0);
        gridPane.add(topicLabel, 0, 1);
        gridPane.add(messageLabel, 0, 2);

        gridPane.add(sendToField, 1, 0);
        gridPane.add(topicField, 1, 1);
        gridPane.add(messageArea, 1, 2);
        gridPane.add(box, 1, 3);
        gridPane.add(toggleBox, 0, 3);

        Tab receiveTab = new Tab("Receive emails");
        receiveTab.setClosable(false);

        tabPane.getTabs().addAll(sendTab, receiveTab);

        button.setOnAction(event -> {

            String message = messageArea.getText();
            String sendTo = sendToField.getText();
            String topic = topicField.getText();

            MailType mailType = (MailType) toggleGroup.getSelectedToggle().getUserData();

            MailService service = MailServiceFactory.getService(mailType);

            service.sendMessage(sendTo, topic, message);

        });

        Scene scene = new Scene(tabPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}