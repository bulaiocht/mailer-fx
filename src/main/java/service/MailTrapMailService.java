package service;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

public class MailTrapMailService
        implements MailService {

    private Session session;
    private Properties properties;

    public MailTrapMailService(Session session,
                               Properties properties) {
        this.session = session;
        this.properties = properties;
    }

    @Override
    public void sendMessage(String to,
                            String topic,
                            String text) {

        MimeMessage message = new MimeMessage(this.session);
        String sender = properties.getProperty("sender.address");

        try {
            message.setFrom(sender);
            message.setRecipients(Message.RecipientType.TO, to);
            message.setSubject(topic);
            message.setSentDate(new Date());

            MimeBodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent(text, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);

            message.setContent(multipart);

            Transport.send(message);

        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }

}
