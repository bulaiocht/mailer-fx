package service;

import config.MailConfigReader;
import config.MailType;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

public class MailServiceFactory {

    public static MailService getService(MailType type) {
        MailConfigReader cr = new MailConfigReader(type);
        switch (type) {
            case MOCK:
                return getMockService(cr);
            case REAL:
                return getRealService(cr);
            default:
                throw new IllegalArgumentException("No such service");
        }
    }

    private static Session initSession(Properties properties) {
        Session instance = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        properties.getProperty("mail.smtp.user"),
                        properties.getProperty("mail.smtp.pass")
                );
            }
        });

        return instance;
    }

    private static MailService getMockService(MailConfigReader configReader) {
        Properties properties = configReader.getProperties();
        Session session = initSession(properties);
        return new MailTrapMailService(session, properties);
    }

    private static MailService getRealService(MailConfigReader configReader) {
        Properties properties = configReader.getProperties();
        Session session = initSession(properties);
        return new GmailMailService(session, properties);
    }

}
