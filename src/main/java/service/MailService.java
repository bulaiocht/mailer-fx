package service;

public interface MailService {

    void sendMessage(String to, String topic, String text);

}