package config;

import java.util.Properties;
import java.util.ResourceBundle;

public class MailConfigReader {

    private static final String REAL = "gmail";
    private static final String MOCK = "mailtrap";

    private Properties properties;

    public MailConfigReader(MailType type) {
        initProperties(type);
    }

    private void initProperties(MailType type) {
        this.properties = new Properties();
        ResourceBundle bundle = chooseBundle(type);
        bundle.keySet().forEach(

                key -> {
                    properties.setProperty(key, bundle.getObject(key).toString());
                }

        );
    }

    private ResourceBundle chooseBundle(MailType type) {
        switch (type) {
            case REAL:
                return ResourceBundle.getBundle(REAL);
            case MOCK:
                return ResourceBundle.getBundle(MOCK);
            default:
                throw new IllegalArgumentException("No such bundle");
        }
    }

    public Properties getProperties() {
        return properties;
    }
}
