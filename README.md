### This is Mailer-3000

#### To start with this project
1. Clone it with ``git clone https://bitbucket.org/bulaiocht/mailer-fx.git``
2. Open terminal in project's root directory
3. Run script ``gradlew`` for Linux or ``gradlew.bat`` for Windows

#### To run your future program
1. Go to ``build/libs/``
2. Run ``java -jar mailer-fx-1.0.jar``